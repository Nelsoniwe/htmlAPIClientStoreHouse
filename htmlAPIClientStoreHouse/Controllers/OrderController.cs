﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace htmlAPIClientStoreHouse.Controllers
{
    public class OrderController : Controller
    {
        public ActionResult GetAll(int id)
        {
            ViewData["CustomerId"] = id;
            return View("GetCustomerOrders");
        }
        
        public ActionResult AddProductToOrder(int id,int cusId)
        {
            
            ViewData["OrderId"] = id;
            ViewData["CustomerId"] = cusId;
            return View("AddProductToOrder");
        }
    }
}
