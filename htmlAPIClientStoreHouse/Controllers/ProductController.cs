﻿using Microsoft.AspNetCore.Mvc;

namespace htmlAPIClientStoreHouse.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult GetAll()
        {
            return View("ListOfProducts");
        }

        public ActionResult FindProductByName()
        {
            return View("FindProductByName");
        }
        public ActionResult EditProductFields(int id)
        {
            ViewData["ProductId"] = id;
            return View("EditProductFields");
        }
        

        public ActionResult AddProduct()
        {
            return View("AddProductFields");
        }
    }
}
