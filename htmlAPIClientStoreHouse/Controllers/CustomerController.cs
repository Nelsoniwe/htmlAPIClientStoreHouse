﻿using Microsoft.AspNetCore.Mvc;

namespace htmlAPIClientStoreHouse.Controllers
{
    public class CustomerController : Controller
    {

        public ActionResult GetAll()
        {
            return View("ListOfCustomers");
        }

        public ActionResult AddCustomerFields()
        {
            return View();
        }
        
        public ActionResult EditCustomerFields(int id)
        {
            ViewData["CustomerId"] = id;
            return View();
        }

        public ActionResult GetCustomerOrders(int id)
        {
            return Redirect("/Order/GetAll/"+id);
        }
    }
}
