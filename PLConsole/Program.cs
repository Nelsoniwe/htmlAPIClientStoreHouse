﻿using System;
using System.Collections.Generic;
using PLConsole.Actions;
using PLConsole.Interfaces;

namespace PLConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isRunning = true;
            IController choosedController = new BaseController();
            
            while (isRunning)
            {
                choosedController.ShowMenu();
                int number;
                if(Int32.TryParse(Console.ReadLine(),out number) && choosedController.Functions.ContainsKey(number))
                {
                    choosedController = choosedController.Functions[number].Invoke();
                }
                else
                {
                    Console.WriteLine("Wrong number");
                }
            }
        }
    }
}