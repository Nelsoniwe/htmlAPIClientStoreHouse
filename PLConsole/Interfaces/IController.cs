﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLConsole.Interfaces
{
    public interface IController
    {
        Dictionary<int, Func<IController>> Functions { get; set; }
        void ShowMenu();
    }
}
