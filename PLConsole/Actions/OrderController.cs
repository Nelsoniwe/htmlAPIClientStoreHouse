﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using PLConsole.Interfaces;
using PLConsole.ObjectsView;

namespace PLConsole.Actions
{
    public class OrderController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        private string uri = "https://localhost:5001/Store";
        //private string uri = "https://localhost:44306/Store";
        public OrderController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = CreateNewOrder,
                [2] = DeleteProductFromOrder,
                [3] = DeleteOrder,
                [4] = GetAllCustomerOrders,
                [5] = AddNewProductToOrder,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Create order" +
                "\n2 - Delete product from order" +
                "\n3 - Delete order" +
                "\n4 - Get customer orders by id" +
                "\n5 - Add product to order" +
                "\n6 - Return");
        }

        public IController CreateNewOrder()
        {
            try
            {
                CustomerView newCustomer = new CustomerView();
                Console.Write("Enter customer Id: ");
                int id;

                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("Wrong number\n" +
                                      "Try Again");
                    return this;
                }

                OrderView order = new OrderView();
                order.customerId = id;

                using (var client = new HttpClient())
                {
                    var response = client.PostAsync(uri + "/Order/Create",
                        new StringContent(JsonSerializer.Serialize(order), Encoding.UTF8, "application/json"));


                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }

                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteProductFromOrder()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();
                var orderDict = new Dictionary<int, OrderView>();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Order/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        int counter = 1;
                        foreach (var item in orders)
                        {
                            orderDict.Add(counter, item);
                            Console.WriteLine(counter++ + ")");
                            Console.WriteLine(" Customer ID: " + item.customerId);
                            Console.WriteLine(" Email: " + item.orderId);
                            Console.WriteLine(" Products: ");
                            int productCounter = 1;
                            foreach (var product in item.orderProducts)
                            {
                                Console.WriteLine("     " + productCounter++ + ")");
                                Console.WriteLine("     ID: " + product.productId);
                                Console.WriteLine("     Name: " + product.productName);
                                Console.WriteLine("     Price: " + product.productPrice);
                                Console.WriteLine("     Count: " + product.count);
                                Console.WriteLine();
                            }

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                        return this;
                    }


                    if (orderDict.Count == 0)
                    {
                        Console.WriteLine("Nothing to delete\n");
                        return this;
                    }



                    Console.Write("Choose Order what do you want delete from: ");
                    int chooseOrderToDeleteId;
                    if (!int.TryParse(Console.ReadLine(), out chooseOrderToDeleteId))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    OrderView choosedOrderToDeleteRealId;
                    if (!orderDict.TryGetValue(chooseOrderToDeleteId, out choosedOrderToDeleteRealId))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }


                    var productsToDelete = choosedOrderToDeleteRealId.orderProducts;
                    var counterProduct = 1;
                    foreach (var product in productsToDelete)
                    {
                        Console.Write($"{counterProduct})    ");
                        Console.WriteLine($"{product.productName}     ");
                        counterProduct++;
                    }

                    Console.Write("Choose Product what do you want delete from: ");
                    int chooseProductToDeleteId;
                    if (int.TryParse(Console.ReadLine(), out chooseProductToDeleteId))
                    {
                        if (!(chooseProductToDeleteId > 0) || !(chooseProductToDeleteId <= counterProduct))
                        {
                            Console.WriteLine("Wrong number\n" +
                                              "Try Again");
                            return this;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    var productToDelete = productsToDelete.ToList()[chooseProductToDeleteId - 1];


                    var responseUpdate = client.PutAsync($"{uri}/Order/RemoveProduct/{choosedOrderToDeleteRealId.orderId}/{productToDelete.productId}", new StringContent(JsonSerializer.Serialize(""), Encoding.UTF8, "application/json"));
                    if (responseUpdate.Result.StatusCode != HttpStatusCode.OK)
                        Console.WriteLine(responseUpdate.Result);
                    else
                        Console.WriteLine("Success");
                    return this;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteOrder()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();
                var orderDict = new Dictionary<int, OrderView>();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Order/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        int counter = 1;
                        foreach (var item in orders)
                        {
                            orderDict.Add(counter, item);
                            Console.WriteLine(counter++ + ")");
                            Console.WriteLine(" Customer ID: " + item.customerId);
                            Console.WriteLine(" Email: " + item.orderId);
                            Console.WriteLine(" Products: ");
                            int productCounter = 1;
                            foreach (var product in item.orderProducts)
                            {
                                Console.WriteLine("     " + productCounter++ + ")");
                                Console.WriteLine("     ID: " + product.productId);
                                Console.WriteLine("     Name: " + product.productName);
                                Console.WriteLine("     Price: " + product.productPrice);
                                Console.WriteLine("     Count: " + product.count);
                                Console.WriteLine();
                            }

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                        return this;
                    }


                    if (orderDict.Count == 0)
                    {
                        Console.WriteLine("Nothing to delete\n");
                        return this;
                    }

                    Console.Write("Choose Order what do you want to delete: ");
                    int chooseOrderToDeleteId;
                    if (!int.TryParse(Console.ReadLine(), out chooseOrderToDeleteId))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    OrderView choosedOrderToDeleteRealId;
                    if (!orderDict.TryGetValue(chooseOrderToDeleteId, out choosedOrderToDeleteRealId))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    var responseDelete = client.DeleteAsync(uri + "/Order/Delete/" + choosedOrderToDeleteRealId.orderId);

                    if (responseDelete.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(responseDelete.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }

                    Console.WriteLine("Success");
                    return this;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetAllCustomerOrders()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Order/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        int counter = 1;
                        foreach (var item in orders)
                        {
                            Console.WriteLine(counter++ + ")");
                            Console.WriteLine(" Customer ID: " + item.customerId);
                            Console.WriteLine(" Email: " + item.orderId);
                            Console.WriteLine(" Products: ");
                            int productCounter = 1;
                            foreach (var product in item.orderProducts)
                            {
                                Console.WriteLine("     " + productCounter++ + ")");
                                Console.WriteLine("     ID: " + product.productId);
                                Console.WriteLine("     Name: " + product.productName);
                                Console.WriteLine("     Price: " + product.productPrice);
                                Console.WriteLine("     Count: " + product.count);
                                Console.WriteLine();
                            }
                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController AddNewProductToOrder()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();
                var orderDict = new Dictionary<int, OrderView>();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Order/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<OrderView> orders = JsonSerializer.Deserialize<List<OrderView>>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        int counter = 1;
                        foreach (var item in orders)
                        {
                            orderDict.Add(counter, item);
                            Console.WriteLine(counter++ + ")");
                            Console.WriteLine(" Customer ID: " + item.customerId);
                            Console.WriteLine(" Email: " + item.orderId);
                            Console.WriteLine(" Products: ");
                            int productCounter = 1;
                            foreach (var product in item.orderProducts)
                            {
                                Console.WriteLine("     " + productCounter++ + ")");
                                Console.WriteLine("     ID: " + product.productId);
                                Console.WriteLine("     Name: " + product.productName);
                                Console.WriteLine("     Price: " + product.productPrice);
                                Console.WriteLine("     Count: " + product.count);
                                Console.WriteLine();
                            }

                            Console.WriteLine();
                        }
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                        return this;
                    }


                    if (orderDict.Count == 0)
                    {
                        Console.WriteLine("Order list is empty\n");
                        return this;
                    }

                    Console.Write("Choose Order what do you want to add product: ");
                    int chooseOrderToAddId;
                    if (!int.TryParse(Console.ReadLine(), out chooseOrderToAddId))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    OrderView choosedOrderToAddReal;
                    if (!orderDict.TryGetValue(chooseOrderToAddId, out choosedOrderToAddReal))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    Console.Write("Type product name: ");
                    string productName = Console.ReadLine();
                    var responseProduct = client.GetAsync(uri + "/Product/Get/" + productName);
                    

                    if (responseProduct.Result.StatusCode != HttpStatusCode.OK)
                    {
                        Console.WriteLine("Product doesn't exist\n" +
                                          "Try Again");
                        return this;
                    }

                    var resProduct = responseProduct.Result.Content.ReadAsStringAsync().Result;
                    ProductView productToAdd = JsonSerializer.Deserialize<ProductView>(resProduct);

                    var responseUpdate = client.PutAsync($"{uri}/Order/AddProduct/{choosedOrderToAddReal.orderId}/{productToAdd.productId}", new StringContent(JsonSerializer.Serialize(""), Encoding.UTF8, "application/json"));
                    if (responseUpdate.Result.StatusCode != HttpStatusCode.OK)
                        Console.WriteLine(responseUpdate.Result);
                    else
                        Console.WriteLine("Success");
                    return this;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }
    }
}
