﻿using System;
using System.Collections.Generic;
using System.Text;
using PLConsole.Interfaces;

namespace PLConsole.Actions
{
    public class BaseController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }

        public BaseController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = GetCustomerController,
                [2] = GetOrderController,
                [3] = GetProductController
            };
        }

        private IController GetCustomerController()
        {
            return new CustomerController();
        }

        private IController GetOrderController()
        {
            return new OrderController();
        }
        private IController GetProductController()
        {
            return new ProductController();
        }


        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to work with: " +
                "\n1 - Customers" +
                "\n2 - Orders" +
                "\n3 - Products");
        }
    }
}
