﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using PLConsole.Interfaces;
using PLConsole.ObjectsView;

namespace PLConsole.Actions
{
    public class ProductController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        private string uri = "https://localhost:5001/Store";
        //private string uri = "https://localhost:44306/Store";
        public ProductController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = CreateProduct,
                [2] = UpdateProductInfo,
                [3] = DeleteProduct,
                [4] = GetProductByName,
                [5] = GetAllProducts,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Create product" +
                "\n2 - Update product info" +
                "\n3 - Delete product" +
                "\n4 - Get product by name" +
                "\n5 - Get all products" +
                "\n6 - Return");
        }

        public IController CreateProduct()
        {
            try
            {
                Console.Write("Enter product name: ");
                ProductView newProduct = new ProductView();
                newProduct.productName = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Product/Get/" + newProduct.productName);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Product already exist\n" +
                                          "Try Again");
                        return this;
                    }

                    Console.Write("Enter product price: ");
                    int price;
                    if (!int.TryParse(Console.ReadLine(), out price))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    newProduct.productPrice = price;
                    Console.Write("Enter product count: ");
                    int count;
                    if (!int.TryParse(Console.ReadLine(), out count))
                    {
                        Console.WriteLine("Wrong number\n" +
                                          "Try Again");
                        return this;
                    }

                    newProduct.count = count;

                    var responseUpdate = client.PostAsync(uri + "/Product/Add",
                        new StringContent(JsonSerializer.Serialize(newProduct), Encoding.UTF8, "application/json"));

                    if (responseUpdate.Result.StatusCode != HttpStatusCode.OK)
                        Console.WriteLine(responseUpdate.Result);
                    else
                        Console.WriteLine("Success");
                    return this;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController UpdateProductInfo()
        {
            try
            {
                Console.Write("Enter product name: ");
                var name = Console.ReadLine();
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Product/Get/" + name);


                    if (response.Result.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Console.WriteLine("Product doesn't exist\n" +
                                          "Try Again");
                        return this;
                    }

                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    ProductView product = JsonSerializer.Deserialize<ProductView>(res);


                    Console.WriteLine("Enter new name, else enter \"skip\"");
                    var newValue = Console.ReadLine();
                    if (!(newValue == "skip" || newValue == ""))
                        product.productName = newValue;
                    Console.WriteLine("Enter new count, else enter \"skip\"");
                    newValue = Console.ReadLine();
                    int newIntCountValue = 0;
                    if (newValue != "skip")
                    {
                        if (!int.TryParse(newValue, out newIntCountValue) || newIntCountValue < 0)
                        {
                            Console.WriteLine("Wrong number\n" +
                                              "Try Again");
                            return this;
                        }

                        product.count = newIntCountValue;

                    }

                    Console.WriteLine("Enter new cost, else enter \"skip\"");
                    newValue = Console.ReadLine();
                    int newIntCostValue = 0;
                    if (newValue != "skip")
                    {
                        if (!int.TryParse(newValue, out newIntCostValue) || newIntCostValue < 0)
                        {
                            Console.WriteLine("Wrong number\n" +
                                              "Try Again");
                            return this;
                        }

                        product.productPrice = newIntCostValue;
                    }


                    var responseUpdate = client.PutAsync(uri + "/Product/Edit", new StringContent(JsonSerializer.Serialize(product), Encoding.UTF8, "application/json"));

                    if (responseUpdate.Result.StatusCode != HttpStatusCode.OK)
                        Console.WriteLine(responseUpdate.Result);
                    else
                        Console.WriteLine("Success");

                    return this;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController DeleteProduct()
        {
            try
            {
                Console.Write("Enter Product Id: ");
                string id = Console.ReadLine();


                using (var client = new HttpClient())
                {
                    var response = client.DeleteAsync(uri + "/Product/Delete/" + id);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }
        public IController GetProductByName()
        {
            try
            {
                Console.Write("Enter Product Name: ");
                string name = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Product/Get/" + name);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    ProductView product = JsonSerializer.Deserialize<ProductView>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("ID: " + product.productId);
                        Console.WriteLine("Name: " + product.productName);
                        Console.WriteLine("Price: " + product.productPrice);
                        Console.WriteLine("Count: " + product.count);
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetAllProducts()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Product/GetAll");
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<ProductView> products = JsonSerializer.Deserialize<List<ProductView>>(res);
                    foreach (var item in products)
                    {
                        Console.WriteLine("ID: " + item.productId);
                        Console.WriteLine("Name: " + item.productName);
                        Console.WriteLine("Price: " + item.productPrice);
                        Console.WriteLine("Count: " + item.count);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }

    }
}
