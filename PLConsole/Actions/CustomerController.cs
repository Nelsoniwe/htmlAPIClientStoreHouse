﻿using System;
using System.Collections.Generic;
using System.Text;
using PLConsole.Interfaces;
using PLConsole.ObjectsView;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Text.Json;

namespace PLConsole.Actions
{
    public class CustomerController : IController
    {
        public Dictionary<int, Func<IController>> Functions { get; set; }
        private string uri = "https://localhost:5001/Store";
        //private string uri = "https://localhost:44306/Store";
        public CustomerController()
        {
            Functions = new Dictionary<int, Func<IController>>()
            {
                [1] = AddCustomer,
                [2] = UpdateCustomerInfo,
                [3] = DeleteCustomerInfo,
                [4] = GetUserInfoById,
                [5] = GetAllCustomers,
                [6] = GetBaseFunc
            };
        }

        public void ShowMenu()
        {
            Console.WriteLine("Choose what you want to do: " +
                "\n1 - Add customer" +
                "\n2 - Update customer info" +
                "\n3 - Delete customer" +
                "\n4 - Get customer info by Id" +
                "\n5 - Get all customers" +
                "\n6 - Return");
        }

        public IController AddCustomer()
        {
            try
            {
                CustomerView customer = new CustomerView();
                Console.Write("Enter customer Email: ");
                string email = Console.ReadLine();
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    Console.WriteLine("Wrong Email. Try Again");
                    return this;
                }

                Console.Write("Enter customer Name: ");
                string name = Console.ReadLine();
                if (name == "")
                {
                    Console.WriteLine("Wrong Name. Try Again");
                    return this;
                }
                customer.customerEmail = email;
                customer.customerName = name;

                using (var client = new HttpClient())
                {
                    var responseUpdate = client.PostAsync(uri + "/Customer/Add",
                        new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json"));
                    if (responseUpdate.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(responseUpdate.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }

                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController UpdateCustomerInfo()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Customer/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    CustomerView customer = JsonSerializer.Deserialize<CustomerView>(res);

                    Console.WriteLine("Enter new name, else enter \"0\"");
                    var newValue = Console.ReadLine();
                    if (!(newValue == "0" || newValue == ""))
                        customer.customerName = newValue;
                    Console.WriteLine("Enter new email, else enter \"0\"");
                    newValue = Console.ReadLine();
                    if (newValue != "0")
                    {
                        if (new EmailAddressAttribute().IsValid(newValue))
                            customer.customerEmail = newValue;
                        else
                            Console.WriteLine("Such Email cannot exit");
                    }


                    var responseUpdate = client.PutAsync(uri + "/Customer/Update", new StringContent(JsonSerializer.Serialize(customer), Encoding.UTF8, "application/json"));

                    if (responseUpdate.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(responseUpdate.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController DeleteCustomerInfo()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.DeleteAsync(uri + "/Customer/Delete/" + id);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("Success");
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetUserInfoById()
        {
            try
            {
                Console.Write("Enter customer Id: ");
                string id = Console.ReadLine();

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Customer/Get/" + id);
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    CustomerView customers = JsonSerializer.Deserialize<CustomerView>(res);

                    if (response.Result.StatusCode == HttpStatusCode.OK)
                    {
                        Console.WriteLine("ID: " + customers.customerId);
                        Console.WriteLine("Email: " + customers.customerEmail);
                        Console.WriteLine("Name: " + customers.customerName);
                        Console.WriteLine();
                    }
                    else
                    {
                        Console.WriteLine(response.Result.Content.ReadAsStringAsync().Result);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }


        public IController GetAllCustomers()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(uri + "/Customer/GetAll");
                    var res = response.Result.Content.ReadAsStringAsync().Result;
                    List<CustomerView> customers = JsonSerializer.Deserialize<List<CustomerView>>(res);
                    foreach (var item in customers)
                    {
                        Console.WriteLine("ID: " + item.customerId);
                        Console.WriteLine("Email: " + item.customerEmail);
                        Console.WriteLine("Name: " + item.customerName);
                        Console.WriteLine();
                    }
                }
                return this;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Try Again");
                return this;
            }
        }

        public IController GetBaseFunc()
        {
            return new BaseController();
        }

    }
}
