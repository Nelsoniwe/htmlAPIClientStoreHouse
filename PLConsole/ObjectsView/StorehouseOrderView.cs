﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLConsole.ObjectsView
{
    public class StorehouseOrderView
    {
        public int orderId { get; set; }
        public int productId { get; set; }
        public int count { get; set; }
        public StoreHouseOrderStatusView status { get; set; } = StoreHouseOrderStatusView.onWay;
    }
}
