﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLConsole.ObjectsView
{
    public class ProductView
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public decimal productPrice { get; set; }
        public int count { get; set; }
    }
}
