﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLConsole.ObjectsView
{
    public class OrderView
    {
        public int orderId { get; set; }
        public int customerId { get; set; }
        public IEnumerable<ProductView> orderProducts { get; set; }
    }
}
