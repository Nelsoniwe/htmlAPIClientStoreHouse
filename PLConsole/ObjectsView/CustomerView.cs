﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PLConsole.ObjectsView
{
    public class CustomerView
    {
        public int customerId { get; set; }
        public string customerName { get; set; }
        public string customerEmail { get; set; }
    }
}
